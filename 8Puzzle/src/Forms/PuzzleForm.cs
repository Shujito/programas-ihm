﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Shujito
{
	class PuzzleForm : Form
	{
		private int cols;
		private int rows;
		private int[] matrix;

		int fontSize;
		Font numberFont;
		bool clickplacement;
		int placenumber;

		Timer t = null;

		public PuzzleForm()
		{
			t = new Timer();
			t.Interval = 2000;
			t.Tick += new EventHandler((object o, EventArgs a) =>
			{
			});

			this.StartPosition = FormStartPosition.CenterScreen;
			this.FormBorderStyle = FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.DoubleBuffered = true;
			this.Text = "Press F1 for Help!";

			// SNES <3
			this.ClientSize = new Size(256, 224);

			// don't do weird stuff here
			this.cols = this.rows = 3;

			this.matrix = new int[rows * cols];

			this.fontSize = 20;
			this.numberFont = new Font(FontFamily.GenericMonospace, this.fontSize);

			this.clickplacement = false;

			// shuffle once!
			this.Shuffle();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);

			for (int col = 0; col < this.cols; col++)
			{
				for (int row = 0; row < this.rows; row++)
				{
					// square sizes
					int width = this.ClientSize.Width / this.cols;
					int height = this.ClientSize.Height / this.rows;
					// starting points
					int posx = width * col;
					int posy = height * row;
					// font positions
					int fontx = posx + (width / 2) - (this.fontSize / 2);
					int fonty = posy + (height / 2) - (this.fontSize / 2);
					// initialize the matrix
					int what = this.matrix[this.rows * row + col];
					// draw a square
					e.Graphics.DrawRectangle(Pens.Black, posx, posy, width, height);
					// it's not a whitespace!
					if (what > 0)
					{
						// draw a square inside
						e.Graphics.DrawRectangle(Pens.Blue, posx + 10, posy + 10, width - 20, height - 20);
						// and a number
						e.Graphics.DrawString(what.ToString(), this.numberFont, Brushes.Red, fontx, fonty);
					}
					// it's unset
					if (what == -1)
					{
						// draw a circle inside
						e.Graphics.DrawEllipse(Pens.Blue, posx + 10, posy + 10, width - 20, height - 20);
						// and a question mark
						e.Graphics.DrawString("?", this.numberFont, Brushes.Red, fontx, fonty);

					}
				}
			}
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			// mouse clicks
			base.OnMouseDown(e);

			for (int col = 0; col < this.cols; col++)
			{
				for (int row = 0; row < this.rows; row++)
				{
					// sizes
					int width = this.ClientSize.Width / this.cols;
					int height = this.ClientSize.Height / this.rows;
					// starting points
					int startx = width * col;
					int starty = height * row;
					// end points
					int endx = startx + width;
					int endy = starty + height;

					// that's the spot
					if (e.X > startx && e.X < endx && e.Y > starty && e.Y < endy)
					{
						// what's the index?
						int index = this.rows * row + col;

						if (clickplacement)
						{
							// are we putting stuff manually?
							if (this.matrix[index] == -1)
							{
								this.matrix[index] = this.placenumber;
								this.placenumber++;
							}

							// are we done?
							if (this.placenumber == this.rows * this.cols)
							{
								// yes we are!
								for (int idx = 0; idx < this.matrix.Length; idx++)
								{
									// now, where's the one cell we didn't chage yet?
									if (this.matrix[idx] == -1)
									{
										// ah there it is
										this.matrix[idx] = 0;
									}
								}
								// we can stop doing this
								this.clickplacement = false;
							}
						}
						else
						{
							// oh we aren't
							int zeroindex = -1;
							for (int idx = 0; idx < this.matrix.Length; idx++)
							{
								// where's zero?
								if (this.matrix[idx] == 0)
									zeroindex = idx;
							}
							int zerocol = zeroindex % this.cols;
							// can I move this?
							bool can = false;

							// can only move previous or next col
							can = (zerocol == col + 1) || can;
							can = (zerocol == col - 1) || can;
							// index must match the zeroindex plus or minus one
							can = (index == zeroindex + 1 || (index == zeroindex - 1)) && can;
							// index can be zeroindex plus or minus total cols
							can = (index == zeroindex + this.cols || index == zeroindex - this.cols) || can;

							// looks like you can actually move this!
							if (can)
							{
								// there's the value
								int value = this.matrix[index];
								// put that where the zero is
								this.matrix[zeroindex] = value;
								// this is the new zero
								this.matrix[index] = 0;
							}
						}
					}
				}
			}

			this.Invalidate();
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			// mouse releases
			base.OnMouseUp(e);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			// mouse moves (duh)
			base.OnMouseMove(e);
			this.Invalidate();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			// press a key
			base.OnKeyDown(e);
			switch (e.KeyCode)
			{
				case Keys.Escape:
					if (this.clickplacement)
					{
						// deactivate and shuffle
						this.clickplacement = false;
						this.Shuffle();
					}
					else
						this.Close();
					break;
				case Keys.F1:
					MessageBox.Show(this, "Press S to shuffle!\nPress P to place numbers manually.\nPress Escape to Quit.", "Help!");
					break;
				case Keys.P:
					// toggle manual placement by click
					this.clickplacement = true;
					for (int idx = 0; idx < this.matrix.Length; idx++)
					{
						// unset all
						this.matrix[idx] = -1;
					}
					// and there's where we start
					this.placenumber = 1;
					break;
				case Keys.S:
					// can't shuffle now
					if (this.clickplacement)
						break;
					// shuffle!
					this.Shuffle();
					break;
			}

			this.Invalidate();
		}

		private void Shuffle()
		{
			// I don't shuffle everyday
			Random rand = new Random(Environment.TickCount);
			// iteration
			for (int idx = 0; idx < this.matrix.Length; idx++)
			{
				// unset all
				this.matrix[idx] = -1;
			}
			// iterations again
			for (int idx = 0; idx < this.matrix.Length; idx++)
			{
				int evaluate = -1;
				// don't repeat numbers
				while (true)
				{
					// make one
					evaluate = rand.Next(0, this.matrix.Length);
					// check if it exists
					bool exists = false;
					for (int jdx = 0; jdx < this.matrix.Length; jdx++)
					{
						if (this.matrix[jdx] == evaluate)
						{
							// it does, keep going
							exists = true;
							break;
						}
					}
					// it doesn't, get out
					if (!exists)
						break;
				}
				// this goes here
				this.matrix[idx] = evaluate;
			}
		}
	}
}