﻿using System.Windows.Forms;

namespace Shujito
{
	class program
	{
		static void Main()
		{
			Application.SetCompatibleTextRenderingDefault(false);
			Application.EnableVisualStyles();
			Application.Run(new PuzzleForm());
		}
	}
}