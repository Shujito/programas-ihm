﻿using System.Collections.Generic;

namespace Shujito.Data
{
	class TreeNode<T>
	{
		#region fields
		List<TreeNode<T>> branches = null;
		#endregion

		#region ctor
		public TreeNode(T value)
		{
			this.branches = new List<TreeNode<T>>();
			this.Value = value;
		}
		#endregion

		#region props
		public T Value
		{ get; private set; }
		#endregion

		#region meth
		public TreeNode<T> getBranch(int index)
		{
			return this.branches[index];
		}

		public int getNumBranches()
		{
			return this.branches.Count;
		}

		public bool isLeaf()
		{
			return this.branches.Count == 0;
		}

		public TreeNode<T> addBranch(T value)
		{
			TreeNode<T> branch = new TreeNode<T>(value);
			this.branches.Add(branch);
			return branch;
		}
		#endregion
	}
}