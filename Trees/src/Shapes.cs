﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Shujito
{
	class Shapes
	{
		#region fields
		private List<Shape> shapes = null;
		#endregion

		#region ctor
		public Shapes()
		{
			this.shapes = new List<Shape>();
		}
		#endregion

		#region props
		public Shape this[int index]
		{
			get
			{
				return this.shapes[index];
			}
		}

		public int Count
		{
			get
			{
				return this.shapes.Count;
			}
		}
		#endregion

		#region meth
		public void addShape(Shape shape)
		{
			this.shapes.Add(shape);
		}

		public void removeShape(Point location, float threshold)
		{
			for (int idx = 0; idx < this.shapes.Count; idx++)
			{
				if ((Math.Abs(location.X - this.shapes[idx].Location.X) < threshold) && (Math.Abs(location.Y - this.shapes[idx].Location.Y) < threshold))
				{
					this.shapes.RemoveAt(idx);

					for (int jdx = idx; jdx < this.shapes.Count; jdx++)
					{
						this.shapes[jdx].ShapeNumber++;
					}

					idx--;
				}
			}
		}
		#endregion
	}
}