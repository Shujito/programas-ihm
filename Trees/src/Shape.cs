﻿using System;
using System.Drawing;

namespace Shujito
{
	class Shape
	{
		#region fields
		
		#endregion

		#region ctor
		public Shape()
		{
		}
		#endregion

		#region props
		public Point Location
		{ get; set; }
		public float Width
		{ get; set; }
		public Color Color
		{ get; set; }
		public int ShapeNumber
		{ get; set; }
		#endregion

		#region meth
		#endregion
	}
}