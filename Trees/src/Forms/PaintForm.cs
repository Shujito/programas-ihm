﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Shujito.Forms
{
	class PaintForm : Form
	{
		#region winforms code
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// PaintForm
			// 
			this.ClientSize = new System.Drawing.Size(624, 442);
			this.DoubleBuffered = true;
			this.Name = "PaintForm";
			this.Text = "Painty";
			this.ResumeLayout(false);

		}
		#endregion

		Shapes shapes = new Shapes();
		bool painting = false;
		bool erasing = false;
		Point lastPos = new Point(0, 0);

		Color currentColor = Color.Black;
		float currentWidth = 10;
		int shapeNum = 0;

		public PaintForm()
		{
			this.InitializeComponent();
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

			for (int idx = 0; idx < this.shapes.Count - 1; idx++)
			{
				Shape shape1 = this.shapes[idx];
				Shape shape2 = this.shapes[idx + 1];

				if (shape1.ShapeNumber == shape2.ShapeNumber)
				{
					using (Pen pen = new Pen(shape1.Color, shape1.Width))
					{
						pen.StartCap = System.Drawing.Drawing2D.LineCap.Round;
						pen.EndCap = System.Drawing.Drawing2D.LineCap.Round;
						e.Graphics.DrawLine(pen, shape1.Location, shape2.Location);
						//e.Graphics.DrawPie(pen, shape1.Location.X, shape2.Location.Y, 3, 3, shape2.Location.X, shape2.Location.Y);
					}
				}
			}
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			this.painting = false;
			this.erasing = false;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			base.OnMouseDown(e);
			this.painting = e.Button == System.Windows.Forms.MouseButtons.Left;
			this.erasing = e.Button == System.Windows.Forms.MouseButtons.Right;
			this.shapeNum++;
			this.lastPos = new Point(0, 0);
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			base.OnMouseMove(e);
			if (this.painting)
			{
				if (this.lastPos != e.Location)
				{
					this.lastPos = e.Location;
					this.shapes.addShape(new Shape()
					{
						Color = this.currentColor,
						Width = this.currentWidth,
						Location = this.lastPos,
						ShapeNumber = this.shapeNum
					});
				}
				this.Invalidate();
			}
			if (this.erasing)
			{
				this.shapes.removeShape(e.Location, 10);
				this.Invalidate();
			}
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			base.OnKeyDown(e);

			switch (e.KeyCode)
			{
				case Keys.Escape: this.Close(); break;
				case Keys.Z: this.currentColor = Color.Red; break;
				case Keys.X: this.currentColor = Color.Green; break;
				case Keys.C: this.currentColor = Color.Blue; break;
				case Keys.V: this.currentColor = Color.Black; break;
				case Keys.A: this.currentColor = Color.White; break;
			}
		}

		protected override void OnMouseWheel(MouseEventArgs e)
		{
			base.OnMouseWheel(e);
			this.currentWidth += e.Delta / 100f;
		}
	}
}