﻿using System;
using System.Drawing;
using System.Collections.Generic;

namespace Shujito.Struct
{
	class Drawable
	{
		#region fields
		List<Drawable> branches = null;
		#endregion

		#region ctor
		public Drawable(string value)
		{
			this.branches = new List<Drawable>();
			this.Value = value;
		}
		#endregion

		#region props
		public string Value
		{ get; set; }
		public Point Location
		{ get; set; }
		public int ShapeNumber
		{ get; set; }
		#endregion

		#region meth
		public Drawable getBranch(int index)
		{
			return this.branches[index];
		}

		public int getNumBranches()
		{
			return this.branches.Count;
		}

		public bool isLeaf()
		{
			return this.branches.Count == 0;
		}

		public Drawable addBranch(string value)
		{
			Drawable branch = new Drawable(value);
			this.branches.Add(branch);
			return branch;
		}
		#endregion
	}
}