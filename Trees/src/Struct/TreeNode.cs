﻿using System;
using System.Collections.Generic;

namespace Shujito.Struct
{
	// source:
	// http://blog.vidasconcurrentes.com/programacion/busqueda-en-profundidad-y-busqueda-en-anchura/
	class TreeNode<T>
	{
		#region fields
		List<TreeNode<T>> branches = null;
		#endregion

		#region ctor
		public TreeNode(T value)
		{
			this.branches = new List<TreeNode<T>>();
			this.Value = value;
		}
		#endregion

		#region props
		public T Value
		{ get; private set; }
		#endregion

		#region meth
		public TreeNode<T> getBranch(int index)
		{
			return this.branches[index];
		}

		public int getNumBranches()
		{
			return this.branches.Count;
		}

		public bool isLeaf()
		{
			return this.branches.Count == 0;
		}

		public TreeNode<T> addBranch(T value)
		{
			TreeNode<T> branch = new TreeNode<T>(value);
			this.branches.Add(branch);
			return branch;
		}

		public TreeNode<T> BFS(T value)
		{
			Queue<TreeNode<T>> aux = new Queue<TreeNode<T>>();
			// add me
			aux.Enqueue(this);

			while (aux.Count != 0)
			{
				TreeNode<T> head = aux.Dequeue();
				Console.WriteLine(head.Value.ToString());

				if (head.Value.Equals(value))
				{
					// I found it!
					return head;
				}
				else
				{
					foreach (TreeNode<T> node in head.branches)
					{
						// add children
						aux.Enqueue(node);
					}
				}
			}

			return null;
		}

		public TreeNode<T> DFS(T value)
		{
			Stack<TreeNode<T>> aux = new Stack<TreeNode<T>>();
			// add me
			aux.Push(this);

			while (aux.Count != 0)
			{
				TreeNode<T> head = aux.Pop();
				Console.WriteLine(head.Value.ToString());

				if (head.Value.Equals(value))
				{
					// I found it!
					return head;
				}
				else
				{
					for (int idx = head.branches.Count - 1; idx >= 0; idx--)
					{
						// add 'em
						aux.Push(head.branches[idx]);
					}
				}
			}

			return null;
		}
		#endregion
	}
}