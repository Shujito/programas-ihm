﻿using System;
using System.Collections.Generic;

namespace Shujito
{
	class DrawableTreeNode
	{
		#region fields
		List<DrawableTreeNode> branches = null;
		#endregion

		#region ctor
		public DrawableTreeNode()
		{
			this.branches = new List<DrawableTreeNode>();
		}
		#endregion

		#region props
		#endregion

		#region meth
		#endregion

	}
}